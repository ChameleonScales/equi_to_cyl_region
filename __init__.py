# ##### BEGIN GPL LICENSE BLOCK #####
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published by
#    the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "Equi to Cyl Region",
	"author": "Caetano Veyssières (ChameleonScales)",
	"version": (0, 4, 0),
	"blender": (2, 80, 0),
	"location": "View3D > Sidebar > Compositor > Camera Cropper",
	"description": "Modifies Camera properties to match the render region",
	"doc_url": "https://gitlab.com/ChameleonScales",
	"tracker_url":"https://gitlab.com/ChameleonScales",
	"warning": "",
	"category": "Node"}

import bpy
import os
from bpy.props import *
from bpy.types import Panel, AddonPreferences
import math

tan = math.tan


def update_Equi2Cyl_category(self, context):
	from bpy.utils import unregister_class
	bpy.utils.unregister_class(EQUI2CYL_PT_compositor_ui)
	EQUI2CYL_PT_compositor_ui.bl_category = self.category
	bpy.utils.register_class(EQUI2CYL_PT_compositor_ui)

def ShowMessageBox(title = "Message Box", icon = 'INFO', lines=""):
    myLines=lines
    def draw(self, context):
        for n in myLines:
            self.layout.label(text=n)
    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

class Equi2CylPreferences(AddonPreferences):
	bl_idname = __name__
	
	category : StringProperty(description="Choose a name for the category of the panel",default="Equi to cyl", update=update_Equi2Cyl_category)

	def draw(self, context):
		layout = self.layout
		box = layout.box()
		row = box.row(align=True)
		row.label(text="Viewport Panel Category:")
		row.prop(self, "category", text="")

class OBJECT_OT_Equi2Cyl(bpy.types.Operator):
	bl_idname = "equi2cyl.scene"
	bl_label = "reproject equi to cyl"
	bl_description = "Convert render projection from equirectangular to central cylindrical"
	bl_options = {'REGISTER', 'UNDO'}
 
	def execute(self, context):
		
		S = bpy.context.scene
		
		# Equirectangular camera attributes
		camName = bpy.context.scene.camera.data.name
		camAttr = bpy.data.cameras[camName].cycles
		
		canvLatmin = camAttr.latitude_min
		canvLatMax = camAttr.latitude_max
		
		# Short names (lowercase "m" means min, uppercase, Max)
		canvX = S.render.resolution_x
		canvY = S.render.resolution_y
		Xm = S.render.border_min_x
		XM = S.render.border_max_x
		Ym = S.render.border_min_y
		YM = S.render.border_max_y
		regY = YM-Ym
		# Canvas latitude range
		laR = canvLatMax - canvLatmin
		# Region latitudes (min and Max)
		regLatm = canvLatmin + Ym*laR
		regLatM = canvLatMax - (1-YM)*laR
		
		try:
			# The canvas height when reprojected to cylinder :
			# REMINDER : using only Max means the canvas has to be vertically symmetrical, but it probably has to regardless. 
			canvCylY = 2 * tan(canvLatMax) * (canvY/laR)

			# Change Scene's render resolution
			S.render.resolution_y = canvCylY

			S.use_nodes = True
	
			N = S.node_tree.nodes
			comp = N['Composite']
			# Socket currently connected to the Compositor output
			out2comp = comp.inputs['Image'].links[0].from_socket
			# add nodes in compositor:
			# Container group:
			containerGrp = N.new('CompositorNodeGroup')
			containerGrp.node_tree = bpy.data.node_groups.new('Equi to Cyl group','CompositorNodeTree')
			containerGrpTree = containerGrp.node_tree
			containerGrpTreeIn = containerGrpTree.nodes.new('NodeGroupInput')
			containerGrpTree.outputs.new('NodeSocketColor','Image')
			containerGrpTreeOut = containerGrpTree.nodes.new('NodeGroupOutput')
			containerGrpTree.inputs.new('NodeSocketColor','Rendered region')
			# group to be assigned Equi-to-cyl template
			E2C = containerGrpTree.nodes.new('CompositorNodeGroup')
			# Transparent canvas image
			canvNode = containerGrpTree.nodes.new('CompositorNodeImage')
	
			# path to the blend file containing template Equi-to-cyl nodegroup:
			templatePath = os.path.dirname(os.path.abspath(__file__))+"/equi2cyl.blend"
			
			# append nodegroup to scene (False means it appends instead of linking):
			with bpy.data.libraries.load(templatePath, link=False) as (data_from, data_to):
			    data_to.node_groups = ["equi-to-cyl"]
			# assign it to the previously added nogegroup:
			E2C.node_tree = bpy.data.node_groups['equi-to-cyl']
			
			# Ensure unique canvas image name
			n = 1
			if 'Canvas' not in bpy.data.images.keys() :
				canvImgName = 'Canvas'
			else:
				while 'Canvas'+ str(n) in bpy.data.images.keys() :
					n += 1
				canvImgName = 'Canvas'+ str(n)
			
			# Generate canvas image and assign it to Canvas node
			bpy.ops.image.new(name=canvImgName, width=canvX, height=canvCylY, alpha=True, color=[0,0,0,0])
			canvasImg = bpy.data.images[canvImgName]
			canvNode.image = canvasImg
	
			# Assign values to the equi-to-cyl nodegroup
			E2C.inputs['region-Y'].default_value = regY * canvY
			E2C.inputs['region-Lat-Max'].default_value = regLatM
			E2C.inputs['region-Lat-min'].default_value = regLatm
			E2C.inputs['canvas-Lat-Max'].default_value = canvLatmin
			E2C.inputs['canvas-Lat-min'].default_value = canvLatMax
			
			# Rearrange and connect nodes:
			compLocX = comp.location[0]
			compLocY = comp.location[1]
			containerGrp.location[0] = compLocX
			containerGrp.location[1] = compLocY
			containerGrpTreeIn.location[0] = -230
			containerGrpTreeIn.location[1] = -50
			containerGrpTreeOut.location[0] = 300
			E2C.width = 200
			E2C.location[1] = 50
			canvNode.location[0] = -230
			canvNode.hide = True
			comp.location[0] += 230
	
			S.node_tree.links.new(out2comp, containerGrp.inputs['Rendered region'])
			containerGrpTree.links.new(containerGrpTreeIn.outputs['Rendered region'], E2C.inputs['Rendered region'])
			
			containerGrpTree.links.new(canvNode.outputs['Image'], E2C.inputs['Canvas'])
			
			containerGrpTree.links.new(E2C.outputs['Image'], containerGrpTreeOut.inputs['Image'])
			S.node_tree.links.new(containerGrp.outputs['Image'], comp.inputs['Image'])
			
			containerGrp.hide = True
			
			# Info report
			self.report({'INFO'}, "Reprojected to cylinder")
		
		except ValueError:
			myLines=("It seems you're trying to reproject a panorama",
				"with a 180° vertical field of view.",
				"This would make an image of infinite height.",
				"Computers don't like that.",
				"Or perhaps did you forget that this add-on",
				"is meant for equirectangular cameras?")
			ShowMessageBox(title="Uh-oh.", icon='ERROR', lines=myLines)

		return {'FINISHED'}


# ------------------------------------------------------
# UI Classes
# ------------------------------------------------------

# Compositor UI
class EQUI2CYL_PT_compositor_ui(Panel):
	bl_idname = "EQUI2CYL_PT_compositor"
	bl_label = "Equi to cyl"
	bl_space_type = "NODE_EDITOR"
	bl_region_type = "UI"
	bl_category = "Equi to cyl"

	def draw(self, context):
		self.layout.row().operator("equi2cyl.scene", icon='SURFACE_NCYLINDER')



# ------------------------------------------------------
# Registration
# ------------------------------------------------------
classes = (
	OBJECT_OT_Equi2Cyl,
	EQUI2CYL_PT_compositor_ui,
	Equi2CylPreferences
)

def register():
	from bpy.utils import register_class
	for cls in classes:
		register_class(cls)

def unregister():
	from bpy.utils import unregister_class
	for cls in reversed(classes):
		unregister_class(cls)


if __name__ == "__main__":
	register()
