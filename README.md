# equi_to_cyl_region

Blender add-on to reproject any region of an equirectangular panorama to a central cylindrical one.

# How to use:
ℹ Your camera has to be equirectangular and vertically centered (latitude min and max must have the same value, negative and positive)
1. go in the compositor
2. open the "N" side panel
3. go in the Equi to cyl tab
4. click on Reproject Equi to cyl
5. Render

Additional feature: 
- Change the panel's tab name from the add-on's preferences. This also allows you to merge it with an existing tab to de-clutter the side panel.

ℹ This add-on can be used in combination with [Camera Cropper](https://gitlab.com/ChameleonScales/camera_cropper)

If you want to do so, you would use the Equi to cyl button while in the Uncropped Scene's compositor.

ℹ This add-on is distribued under the GPL version 3 license.